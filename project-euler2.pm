# Euler 2
# Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:
# 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
# By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

#!usr/bin/perl -w
use strict;
use warnings;

package Solution;
# _-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-
our ($current, $previous, $next, $limit, $even) = (1, 1, 0, 4_000_000, 0);
our $index = 0;

while ($current < $limit) {
  $next = $current + $previous;
  $previous = $current;
  $even += $current if ($current % 2 == 0);
  
  print "[".$current."]"."\t";

  $current = $next;
}

print "\n== Answer ==\n";
print $even."\n";
# _-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-_-*-
