# A number is divisible by 2  if the last digit is 0, 2, 4, 6 or 8.
# A number is divisible by 3  if the sum of the digits is divisible by 3.
# A number is divisible by 4  if the number formed by the last two digits is divisible by 4.
# A number is divisible by 5  if the last digit is either 0 or 5.
# A number is divisible by 6  if it is divisible by 2 AND it is divisible by 3.
# A number is divisible by 8  if the number formed by the last three digits is divisible by 8.
# A number is divisible by 9  if the sum of the digits is divisible by 9.
# A number is divisible by 10  if the last digit is 0. 
# _-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-_-*-_-
#!usr/bin/perl -w
use strict;
use warnings;

# prompt user
print "Enter a number to be factorized: ";
our $input = <>;
our @factors = ();

# validation
if ($input < 2) { print "No prime factors!"; return; }

# processing
print "Processing...\n";
for my $index (2,3,5) 
{ 
    $input = solution($input, $index); 
}

sub solution {
 my ($dividen, $divisor) = array(@_);
 if ($dividen % $divisor != 0) { return $input; }
 solution($dividen / $divisor);
}

# output
print $input;
