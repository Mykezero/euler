# Euler 1
# -----------------------------------------------------------------------------
# If we list all the natural numbers below 10 that are multiples of 3 or 5, 
# we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.
# -----------------------------------------------------------------------------
# Natural Numbers below 11
# 3 6 9 	-> multiples of 3
# 5 10  	-> multiples of 5
# -----------------------------------------------------------------------------

#! usr/bin/perl -w
use strict;
use warnings;

my $summation = 0;

for(0 .. 999) {
  if ($_ % 3 == 0 || $_ % 5 == 0) {
    $summation += $_;
  }
}

print $summation;
